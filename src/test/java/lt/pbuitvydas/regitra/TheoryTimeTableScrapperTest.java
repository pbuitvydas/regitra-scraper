package lt.pbuitvydas.regitra;
import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

public class TheoryTimeTableScrapperTest {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyy.MM.dd");
	private String dateFromStr = "2014.09.7";
	private String dateTillStr = "2014.09.22";

	@Test
	public void test() throws Exception {
		String url = "https://www.eregitra.lt/viesa/interv/INT_GRAFIKAS_VIEW_T.php?Padalinys=KN&Action=";
		List<Date> dates = null;
		while (true) {
			dates = execute(url);
			if (dates.isEmpty()) {
				Thread.sleep(1000 * 60);
			} else {
				break;
			}
		}
		openBrowser(url);
		Assert.assertFalse(dates.isEmpty());
	}
	
	private List<Date> execute(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		Assert.assertNotNull(doc);
		List<Date> dates = getAvailiableDates(doc);
		System.out.println("Availiable dates:");
		System.out.println(printDates(dates));
		dates = getPrefferedDates(dates);
		System.out.println("Preffered dates:");
		System.out.println(printDates(dates));
		return dates;
	}
	
	private List<Date> getAvailiableDates(Document doc) {
		List<Date> dates = new ArrayList<Date>();
		for (Element e : doc.select("th[width=180]")) {
			Date d = parseDateFromHtml(e.text());
			if (d != null) {
				dates.add(d);
			}
		}
		return dates;
	}
	
	private Date parseDateFromHtml(String str) {
		int idx = str.indexOf(" ");
		str = str.substring(idx + 1);
		try {
			return DATE_FORMAT.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private Date parseDate(String str) {
		try {
			return DATE_FORMAT.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private List<Date> getPrefferedDates(List<Date> availiableDates) {
		Date dateFrom = parseDate(dateFromStr);
		Date dateTill = parseDate(dateTillStr);
		List<Date> prefferedDates = new ArrayList<Date>();
		for (Date d : availiableDates) {
			if (d.after(dateFrom) && d.before(dateTill)) {
				prefferedDates.add(d);
			}
		}
		return prefferedDates;
	}
	
	private String printDates(List<Date> dates) {
		String str = "";
		for (Date d : dates) {
			str += DATE_FORMAT.format(d) + "\n";
		}
		return str;
	}
	
	private void openBrowser(String url) throws Exception {
	    Desktop desktop = Desktop.getDesktop();
	    if (desktop.isSupported(Action.BROWSE)) {
	        desktop.browse(new URL(url).toURI());
	    }
	}
  
}
